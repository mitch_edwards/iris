"""
DTIrisHunt.py

"""


import requests, json, sys 

def setupAPI():
    try:
        f = open('dtapikey.txt','r')
        username = f.readline().strip('\n')
        apikey = f.readline().strip('\n')
        f.close()
    except:
        username = input('[-] dtapikey.txt not found, please enter Domain Tools Username: ')
        apikey = input('[-] Domain Tools API Key: ')
        f = open('dtapikey.txt','w')
        f.write(username+'\n')
        f.write(apikey)

        f.close()

    return username, apikey

def checkdt(domain, dtuser, dtapi):
    #TODO
    dtreport = ''
    dtreport += '[-] DomainTools report on domain '+domain+'\n'


    url = 'https://api.domaintools.com/v1/iris-investigate/'
    last = '?api_username='+dtuser+'&api_key='+dtapi
    results = requests.get(url+'iris-investigate/'+last+'&domain='+domain)
    results_obj = json.loads(results.text)['response']


    # print(dtreport)
    # print(results_obj)

    resultscount = results_obj['results_count']
    results = results_obj['results']
    return dtreport
def checktc(domain):
    tcreport = ''
    tcreport += '[-] ThreatConnect report on domain '+domain+'\n'
    return ''

def parseargs():
    args = sys.argv
    parsedargs = {}
    if '--checktc' in args:
        parsedargs['checktc'] = True 
    else:
        parsedargs['checktc'] = False
    if '--domain' in args:
        parsedargs['isdomain'] = True 
        index = 0
        for arg in args:
            if arg == '--domain':
                parsedargs['domain'] = args[index+1]
            else:
                index = index+1
    else:
        parsedargs['isdomain'] = False
    if not parsedargs['isdomain'] or '--help' in args:
        parsedargs['ishelp'] = True
    else:
        parsedargs['ishelp'] = False
    return parsedargs

def main():
    username, apikey = setupAPI()
    url = 'https://api.domaintools.com/v1/'
    last = '?api_username='+username+'&api_key='+apikey

    args = parseargs()
    print(str(args))
    if args['ishelp']:
        print('[x] Help')
    if args['isdomain']:
        print(checkdt(args['domain'], username, apikey))
        if args['checktc']:
            print(checktc(args['domain']))

main()